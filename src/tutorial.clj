(ns tutorial
  (:require [datomic.client.api :as d]))


;; client api tutorial from datomic.com
(def cfg {:server-type :datomic-local
          :system "datomic-samples"})

(def client (d/client cfg))

(d/create-database client {:db-name "movies"}) ;; notice that this will create files in the directory where you defined in you .datomic/local.edn

(def conn (d/connect client {:db-name "movies"})) 

(def movie-schema [{:db/ident :movie/title
                    :db/valueType :db.type/string
                    :db/cardinality :db.cardinality/one
                    :db/doc "The title of the movie"}
                   {:db/ident :movie/genre
                    :db/valueType :db.type/string
                    :db/cardinality :db.cardinality/one
                    :db/doc "The genre of the movie"}
                   {:db/ident :movie/release-year
                    :db/valueType :db.type/long
                    :db/cardinality :db.cardinality/one
                    :db/doc "The year the movie was released in the theaters"}])

(d/transact conn {:tx-data movie-schema})

(def first-movies [{:movie/title "The Goonies"
                    :movie/genre "action/adventure"
                    :movie/release-year 1985}
                   {:movie/title "Commando"
                    :movie/genre "thriller/action"
                    :movie/release-year 1985}
                   {:movie/title "Repo Man"
                    :movie/genre "punk dystopia"
                    :movie/release-year 1984}])

first-movies

(def first-movies-tx (d/transact conn {:tx-data first-movies}))

first-movies-tx

(def tempdb (d/db conn)) ;; it is (always) a snapshot


(def query
  "retrieves all movie titles"
  '[:find ?movie-title
    :where [_ :movie/title ?movie-title]])

(d/q query tempdb)


;; end of datomic.com tutorial

;; now, some cool stuff from RH's talk (The Functional Database - Rich Hickey)


;; let's say I want to add the new dune movie

(def dune-tx [{:movie/title "Dune" :movie/genre "action" :movie/release-year 2024}])

(def dune-db (-> (d/db conn)
                 (d/with dune-tx)
                 :db-after))

(d/q query dune-db)
;; (keys dune-tx)

;; (= (:db-after dune-tx) tempdb) ;; should be false

;; (= (:db-before dune-tx) tempdb) ;; should be true

;; (def dune-db (d/db conn))

;; (d/q query (:db-after dune-tx))


